
        import java.lang.reflect.Array;
        import java.util.*;

public class TreeNode implements Iterator<TreeNode> {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode (String n, TreeNode d, TreeNode r) {
      this.setName(n);
      this.setFirstChild(d);
      this.setNextSibling(r);
      // TODO!!! Your constructor here
   }
   public void setName(String s) {
      name = s;
   }
   public String getName() {
      return name;
   }
   public void setNextSibling (TreeNode p) {
      nextSibling = p;
   }
   public TreeNode getNextSibling() {
      return nextSibling;
   }
   public void setFirstChild (TreeNode a) {
      firstChild = a;
   }

   public boolean hasNext() {
      return (getNextSibling() != null);
   }

   public boolean hasChild() {
      return !(this.firstChild== null);
   }

   public TreeNode next() {
      return getNextSibling();
   }
   public static TreeNode parsePrefix (String s) {
      //Test if s is empty
      if (s.isEmpty()){throw new RuntimeException("Empty input");}
      //test if s has empty brackets
      if(s.contains("()")){throw new RuntimeException("Some empty brackets i TreeNode: "+s);}
      //parse name
      StringTokenizer st= new StringTokenizer(s,"(,)",true);
      String nodeName=new String();
      ArrayList<String> symbols=new ArrayList();
      while (st.hasMoreElements()){
         symbols.add(st.nextToken());
      }
      if(symbols.get(0).equals("")){throw new RuntimeException("Cannot find TreeNode name!");}
      else if (!(symbols.get(0).contains("(")||symbols.get(0).contains(")")||symbols.get(0).equals(",")||symbols.get(0).contains(" ")||symbols.get(0).contains("\t"))) {
         nodeName=symbols.get(0);}
     else{throw new RuntimeException("Illegal characters in TreeNode name: " + nodeName);}
      symbols.remove(0);
      //test TreeNode format
      int count1=0;
      int count2=0;
      for (int i = 0; i <symbols.size() ; i++) {
         if (symbols.get(i).equals("(")){
         count1++;
      }else if(symbols.get(i).equals(")")){
            count2++;
         }
      }
      if (symbols.size()>1 && count1==0 && count2==0){throw new RuntimeException("TreeNode without brackets: " + s);}
      if (symbols.size()>1 && count1!=count2){throw new RuntimeException("Invalid bracket combination: " + s);}
      //parse children
      String temp=new String();
      for (int i = 0; i <symbols.size() ; i++) {
         temp+=symbols.get(i);
      }
      List<String> children = new ArrayList<String>();
      int j = 0;
      int k = 0;
      if (temp.length() > 0 && temp.charAt(0) == '(') {
      for (int i = 0; i<temp.length(); i++) {
         if (temp.charAt(i) == '(') {
            j++;
         } else if (temp.charAt(i) == ')') {
            if (j==0) {
               break;
            } else if (j==1) {
               children.add(temp.substring(k+1, i));
               k = i;
            } else {
               j--;
            }
         } else if (temp.charAt(i) == ',' && j==1) {
            children.add(temp.substring(k+1, i));
            k = i;
         }
      }
      } else { return new TreeNode(nodeName, null, null);}
      //TreeNode child
      TreeNode child = null;
      for (int l = children.size() - 1; l >= 0; l--) {
         TreeNode tmp = TreeNode.parsePrefix(children.get(l));
         tmp.nextSibling = child;
         child = tmp;
      }
      return new TreeNode(nodeName, child, null);
      // TODO!!! return the root
   }

   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      b.append(name);

      if (this.hasNext()) {
         b.append(",");
         b.append(this.nextSibling.rightParentheticRepresentation());
      }

      if (this.hasChild()) {
         b.insert(0, ")");
         b.insert(0, this.firstChild.rightParentheticRepresentation());
         b.insert(0, "(");
      }
      // TODO!!! create the result in buffer b
      return b.toString();
   }

   public static void main (String[] param) {
      String s = "A(B(C,D(E)),F) ";

      TreeNode t = TreeNode.parsePrefix (s);
      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}




